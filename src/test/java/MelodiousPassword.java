import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static java.util.Collections.singletonList;

/**
 * Created by ccr on 17/03/2017.
 */
public class MelodiousPassword {

    public static final char[] vowels = new char[]{'a', 'e', 'i', 'o', 'u'};
    public static final char[] consonants = new char[]{'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'};

    public static List<String> gen(int i, List<String> layer, char [] current, char [] next) {
        if(i == 0) return layer;
        List<String> result = new ArrayList<String>();
        for (String s : layer) {
            for (char c: current) {
                result.add(format("%s%c",s,c));
            }
        }
        return gen(i - 1,result, next, current);
    }

    public static List<String> passwords(int n) {
        if( n < 1 || n > 6) throw new IllegalArgumentException();
        List<String> passwords = new ArrayList<String>();
        passwords.addAll(gen(n, singletonList(""),vowels, consonants));
        passwords.addAll(gen(n, singletonList(""),consonants, vowels));
        return passwords;
    }

    public static void main(String[] args) {
        for(String pwd : passwords(4))
            System.out.println(pwd);
    }
}
