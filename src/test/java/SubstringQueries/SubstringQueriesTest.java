package SubstringQueries;

import SubstringQueries.Solution.LongestSubstringFinder;
import SubstringQueries.Solution.SuffixTree;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SubstringQueriesTest {
    @Test
    public void longestCommonSubstringLength_of_hello_and_x_should_be_0() {
        LongestSubstringFinder finder = new LongestSubstringFinder(null);
        assertThat(finder.longestCommonSubstringLength("hello", "x")).isEqualTo(0);
    }

    //@Test
    public void longestCommonSubstringLength_of_hello_and_hello_should_be_5() {
        LongestSubstringFinder finder = new LongestSubstringFinder(null);
        String s = "hello";
        assertThat(finder.longestCommonSubstringLength(s, s)).isEqualTo(s.length());
    }

    @Test
    public void suffix_tree_of_hello_should_not_contain_x() {
        SuffixTree tree = new SuffixTree("hello");
        assertThat(tree.containsSubstring("x")).isFalse();
    }

    //@Test
    public void suffix_tree_of_hello_should_contain_el() {
        SuffixTree tree = new SuffixTree("hello");
        assertThat(tree.containsSubstring("el")).isTrue();
    }

    @Test
    public void suffixes_of_hello_should_be_o_lo_llo_ello_hello() {
        String[] suffixes = SuffixTree.suffixes("hello");
        assertThat(suffixes).hasSize(5);
        assertThat(suffixes).containsOnly("o", "lo", "llo", "ello", "hello");
    }

    /*
            hello

                ROOT
            O   L
               O L
                  O
        add("llo")
        get("ll") -> .childs.size() == 1
     */


    @Test
    public void get_emptyString_should_return_root_node_with_empty_childs_when_added_nothing() {
        SuffixTree suffixTree = new SuffixTree();
        assertThat(suffixTree.get("").getChildren()).isEmpty();
    }

    @Test
    public void root_should_have_one_child_with_empty_children_when_added_single_letter_suffix() {
        SuffixTree suffixTree = new SuffixTree();
        suffixTree.add("o");
        assertThat(suffixTree.get("").getChildren()).hasSize(1);
        assertThat(suffixTree.get("o").getChildren()).isEmpty();
    }




}
