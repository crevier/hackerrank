package SubstringQueries;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int q = in.nextInt();
        String[] s = new String[n];
        for(int s_i=0; s_i < n; s_i++){
            s[s_i] = in.next();
        }
        for(int a0 = 0; a0 < q; a0++){
            int x = in.nextInt();
            int y = in.nextInt();
            // your code goes here
        }
    }

    static class Input {
        // strings
        // queries
    }

    static class LongestSubstringFinder {
        LongestSubstringFinder(Input input) {
            // build useful data structures and initialize caches
        }

        int longestCommonSubstringLength(String a, String b) {
            return 0;
        }
    }

    /**
     * The suffix tree for a word
     */
    static class SuffixTree {

        private Node root = new Node();


        SuffixTree(String word) {
            for (String suffix : suffixes(word)) {
                //add(suffix);
            }
        }

        public SuffixTree() {
            
        }

        static String[] suffixes(String word) {
            String[] suffixes = new String[word.length()];
            for (int i = 0; i < word.length(); i++) {
                suffixes[i] = word.substring(i);
            }
            return suffixes;
        }

        boolean containsSubstring(String sub) {
            return false;
        }

        public Node get(String s) {
            if(s.isEmpty())
                return root;
            return root.getChildren().get(s.charAt(0));
        }

        public void add(String s) {

            root.getChildren().computeIfAbsent(s.charAt(0),
                        key -> new Node()
                    );

        }

        static class Node {

            private Map<Character, Node> children = new HashMap<Character, Node>();

            public Map<Character,Node> getChildren() {
                return children;
            }
        }
    }
}
